package com.vender98.personalaccount.constants;

import android.graphics.Color;

public class Constants {
    private static final String ENDPOINT_LOCALHOST = "http://172.18.5.53:8080";
    private static final String ENDPOINT_HEROKU = "https://telecom-web-service.herokuapp.com";
    public static final int SPLASH_TIME_OUT = 500;
    public static final int CHART_ROTATION_ANGLE = 270;
    public static final int DEFAULT_RANGE_SIZE = 3;
    public static final int SPENDING_HISTORY_AVAILABLE_PERIOD_MONTHS = 3;
    public static final int VISIBLE_TRESHOLD = 3;
    public static final int HTTP_UNPROCESSABLE_ENTITY = 422;
    public static final int SMS_CONFIRMATION_TIMEOUT = 30000;
    public static final int SMS_CONFIRMATION_DELAY = 1000;


    //Chart constants
    public static final int REMAINING_PART_OF_CHART_COLOR = Color.CYAN;
    public static final int EXPIRED_PART_OF_CHART_COLOR = Color.LTGRAY;
    public static final int LABEL_CHART_COLOR = Color.RED;
    public static final int LABEL_CHART_FONT_SIZE = 10;
    public static final int CHART_COLORS_COUNT = 6;
    public static final int[] CHART_COLORS = {
            Color.CYAN,
            Color.GREEN,
            Color.YELLOW,
            Color.MAGENTA,
            Color.RED,
            Color.BLUE
    };

    //API param names
    public static final String BALANCE_KEY = "balance";


    //Model map keys
    public static final String CUSTOMER_ACCOUNT_KEY = "customerAccount";
    public static final String CUSTOMER_TARIFF_PLAN_KEY = "customerTariffPlan";
    public static final String HAS_AVAILABLE_TARIFF_PLANS_FLAG_KEY = "hasAvailableTariffPlans";
    public static final String HAS_AVAILABLE_SPENDING_ITEM_HISTORY_FLAG_KEY = "hasAvailableSpendingItemHistory";
    public static final String TARIFF_PLANS_KEY = "tariffPlans";
    public static final String SPENDING_DETAILED_ITEMS = "spendingDetailedItems";

    //Characteristic types
    public static final String SMS = "SMS";
    public static final String INTERNET = "internet";
    public static final String MINUTES = "minutes";

    //Formatting patterns
    public static final String BALANCE_LABEL_PATTERN = "#0.##";
    public static final String GENERAL_DATE_PATTERN = "dd.MM.yyyy";
    public static final String ITEM_DATE_PATTERN = "dd MMMM";
    public static final String ITEM_TIME_PATTERN = "HH:mm";

    //Endpoint for connection with Web Service
    public static final String ENDPOINT = ENDPOINT_HEROKU;
}
