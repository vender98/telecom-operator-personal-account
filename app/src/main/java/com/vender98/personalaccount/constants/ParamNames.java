package com.vender98.personalaccount.constants;

public class ParamNames {
    public static final String FROM_KEY = "from";
    public static final String TO_KEY = "to";
    public static final String SPENDING_ITEM_ID = "spendingItemId";
}
