package com.vender98.personalaccount.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.vender98.personalaccount.App;

import org.apache.commons.lang3.StringUtils;

public class UserProfile {
    private Context context;
    private SharedPreferences mSettings;

    private final static String TOKEN_KEY = "TOKEN";
    private final static String STORAGE_KEY = "PROFILE_STORAGE";
    private static final String LINE_NUMBER_KEY = "LINE_NUMBER";

    /**
     * Constructor
     */
    public UserProfile(App app) {
        mSettings = app.getSharedPreferences(STORAGE_KEY, Context.MODE_PRIVATE);
    }

    public boolean isAuthorized() {
        return (StringUtils.isNotEmpty(getToken()));
    }

    public boolean setToken(String value) {
        return setValue(TOKEN_KEY, value);
    }

    public String getToken() {
        return mSettings.getString(TOKEN_KEY, null);
    }

    public boolean setLineNumber(Long id) {
        return setValue(LINE_NUMBER_KEY, id);
    }

    public Long getLineNumber() {
        return mSettings.getLong (LINE_NUMBER_KEY, 0);
    }

    public boolean clearLineNumber() {
        return removeValue(LINE_NUMBER_KEY);
    }

    public boolean clearToken() {
        return removeValue(TOKEN_KEY);
    }

    public boolean cleanup() {
        SharedPreferences.Editor editor = mSettings.edit();
        return editor.clear().commit();
    }

    protected int getIntegerValue(String key, int defaultValue) {
        return mSettings.getInt(key, defaultValue);
    }

    protected boolean getBooleanValue(String key, boolean defaultValue) {
        return mSettings.getBoolean(key, defaultValue);
    }

    protected Long getLongValue(String key, Long defaultValue) {
        return mSettings.getLong(key, defaultValue);
    }

    protected String getStringValue(String key, String defaultValue) {
        return mSettings.getString(key, defaultValue);
    }

    protected boolean setValue(String key, String value) {
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    protected boolean setValue(String key, boolean value) {
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    protected boolean setValue(String key, long value) {
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    protected boolean setValue(String key, int value) {
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    protected boolean removeValue(String key) {
        SharedPreferences.Editor editor = mSettings.edit();
        editor.remove(key);
        return editor.commit();
    }


}