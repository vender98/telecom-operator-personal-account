package com.vender98.personalaccount.model;

import java.sql.Timestamp;
import java.util.List;

public class SpendingDetailedItemsByDate {
    private Timestamp date;
    private List<SpendingItemDetailed> mSpendingItemDetailedList;

    public SpendingDetailedItemsByDate(Timestamp date, List<SpendingItemDetailed> spendingItemDetailedList) {
        this.date = date;
        mSpendingItemDetailedList = spendingItemDetailedList;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public List<SpendingItemDetailed> getSpendingItemDetailedList() {
        return mSpendingItemDetailedList;
    }

    public void setSpendingItemDetailedList(List<SpendingItemDetailed> spendingItemDetailedList) {
        mSpendingItemDetailedList = spendingItemDetailedList;
    }
}
