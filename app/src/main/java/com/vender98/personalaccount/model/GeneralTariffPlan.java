package com.vender98.personalaccount.model;

import java.io.Serializable;
import java.util.List;

public class GeneralTariffPlan implements Serializable {
    private long id;
    private String name;
    private String price;
    private List<Characteristic> characteristics;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public List<Characteristic> getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(List<Characteristic> characteristics) {
        this.characteristics = characteristics;
    }
}
