package com.vender98.personalaccount.services;

import com.vender98.personalaccount.model.CustomerAccount;
import com.vender98.personalaccount.model.CustomerTariffPlan;
import com.vender98.personalaccount.model.GeneralTariffPlan;
import com.vender98.personalaccount.model.SpendingItem;
import com.vender98.personalaccount.model.SpendingItemDetailed;

import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestAPI {
    String AUTHORIZATION_HEADER = "Authorization";

    @GET("/customerAccount/{lineNumber}")
    Single<CustomerAccount> getCustomerAccount(@Header(AUTHORIZATION_HEADER) String token,
                                               @Path("lineNumber") long lineNumber);

    @GET("/customerAccount/{lineNumber}/tariffPlan")
    Single<CustomerTariffPlan> getCustomerTariffPlan(@Header(AUTHORIZATION_HEADER) String token,
                                                     @Path("lineNumber") long lineNumber);

    @POST("/customerAccount/{lineNumber}/recharge")
    Single<Map<String, Integer>> rechargeCustomerAccount(@Header(AUTHORIZATION_HEADER) String token,
                                                        @Path("lineNumber") long lineNumber,
                                                        @Query("amount") int amount);
    @GET("/customerAccount/{lineNumber}/tariffPlanDetailed")
    Single<GeneralTariffPlan> getGeneralTariffPlan(@Header(AUTHORIZATION_HEADER) String token,
                                                   @Path("lineNumber") long lineNumber);

    @GET("/customerAccount/{lineNumber}/tariffPlansAvailable")
    Single<List<GeneralTariffPlan>> getAvailableTariffPlans(@Header(AUTHORIZATION_HEADER) String token,
                                                            @Path("lineNumber") long lineNumber,
                                                            @Query("range") int range);

    @POST("customerAccount/{lineNumber}/changeTariffPlan")
    Single<CustomerTariffPlan> changeTariffPlan(@Header(AUTHORIZATION_HEADER) String token,
                                                @Path("lineNumber") long lineNumber,
                                                @Query("tariffPlanId") long tariffPlanId);

    @GET("customerAccount/{lineNumber}/hasAvailableTariffPlans")
    Single<Map<String, Boolean>> hasAvailableTariffPlans(@Header(AUTHORIZATION_HEADER) String token,
                                                         @Path("lineNumber") long lineNumber,
                                                         @Query("range") int range);

    @GET("customerAccount/{lineNumber}/spendingHistory")
    Single<List<SpendingItem>> getSpendingHistory(@Header(AUTHORIZATION_HEADER) String token,
                                                  @Path("lineNumber") long lineNumber,
                                                  @Query("from") long from,
                                                  @Query("to") long to);

    @GET("customerAccount/{lineNumber}/spendingHistory/{spendingItemId}")
    Single<List<SpendingItemDetailed>> getItemSpendingHistory(@Header(AUTHORIZATION_HEADER) String token,
                                                              @Path("lineNumber") long lineNumber,
                                                              @Path("spendingItemId") long spendingItemId,
                                                              @Query("from") long from,
                                                              @Query("to") long to,
                                                              @Query("range") int range);

    @GET("customerAccount/{lineNumber}/hasAvailableSpendingItemHistory/{spendingItemId}")
    Single<Map<String, Boolean>> hasAvailableSpendingItemHistory(@Header(AUTHORIZATION_HEADER) String token,
                                                                 @Path("lineNumber") long lineNumber,
                                                                 @Path("spendingItemId") long spendingItemId,
                                                                 @Query("from") long from,
                                                                 @Query("to") long to,
                                                                 @Query("range") int range);

    @POST("authentication/generateCode")
    Single<String> sendSmsCode(@Query("lineNumber") long lineNumber);

    @PUT("authentication/{token}/resendCode")
    Completable resendSmsCode(@Path("token") String token);

    @PUT("authentication/{token}/verifyCode")
    Completable verifySmsCode(@Path("token") String token,
                              @Query("code") String code);

    @DELETE("authentication/{token}/logout")
    Completable logout(@Path("token") String token);

    @GET("authentication/{token}/isValid")
    Completable isTokenValid(@Path("token") String token);
}
