package com.vender98.personalaccount.events;

import com.vender98.personalaccount.model.GeneralTariffPlan;

import java.util.List;

public class ShowMorePlansEvent {
    List<GeneralTariffPlan> tariffPlans;
    boolean hasAvailableTariffPlans;

    public ShowMorePlansEvent(List<GeneralTariffPlan> tariffPlans, boolean hasAvailableTariffPlans) {
        this.tariffPlans = tariffPlans;
        this.hasAvailableTariffPlans = hasAvailableTariffPlans;
    }

    public List<GeneralTariffPlan> getTariffPlans() {
        return tariffPlans;
    }

    public boolean hasAvailableTariffPlans() {
        return hasAvailableTariffPlans;
    }
}
