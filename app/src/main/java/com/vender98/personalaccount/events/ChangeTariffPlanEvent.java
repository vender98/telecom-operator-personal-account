package com.vender98.personalaccount.events;

import com.vender98.personalaccount.model.CustomerAccount;
import com.vender98.personalaccount.model.CustomerTariffPlan;

public class ChangeTariffPlanEvent {
    private CustomerTariffPlan tariffPlan;
    private CustomerAccount customerAccount;

    public ChangeTariffPlanEvent(CustomerTariffPlan tariffPlan, CustomerAccount customerAccount) {
        this.tariffPlan = tariffPlan;
        this.customerAccount = customerAccount;
    }

    public CustomerTariffPlan getTariffPlan() {
        return tariffPlan;
    }

    public CustomerAccount getCustomerAccount() {
        return customerAccount;
    }
}
