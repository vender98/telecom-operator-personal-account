package com.vender98.personalaccount.events;

public class AmountChangedEvent {
    private int amount;

    public AmountChangedEvent(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }
}
