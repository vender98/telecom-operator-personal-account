package com.vender98.personalaccount.events;

import android.os.Handler;
import android.os.Looper;
import com.squareup.otto.Bus;
import java.util.ArrayList;

public class AndroidBus extends Bus {
    private final Handler mainThread = new Handler(Looper.getMainLooper());
    private final ArrayList<Object> registeredObjects = new ArrayList<>();

    @Override
    public void post(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        } else {
            mainThread.post(() -> AndroidBus.super.post(event));
        }
    }

    @Override
    public void register(Object object) {
        if (!registeredObjects.contains(object)) {
            registeredObjects.add(object);
            super.register(object);
        }
    }

    @Override
    public void unregister(Object object) {
        if (registeredObjects.contains(object)) {
            registeredObjects.remove(object);
            super.unregister(object);
        }
    }
}