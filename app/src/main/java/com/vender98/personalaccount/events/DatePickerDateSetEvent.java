package com.vender98.personalaccount.events;

import com.vender98.personalaccount.constants.DatePickerOpenTime;

public class DatePickerDateSetEvent {
    private long from;
    private long to;
    private DatePickerOpenTime openTime;

    public DatePickerDateSetEvent(long from, long to, DatePickerOpenTime openTime) {
        this.from = from;
        this.to = to;
        this.openTime = openTime;
    }

    public long getFrom() {
        return from;
    }

    public long getTo() {
        return to;
    }

    public DatePickerOpenTime getOpenTime() {
        return openTime;
    }
}
