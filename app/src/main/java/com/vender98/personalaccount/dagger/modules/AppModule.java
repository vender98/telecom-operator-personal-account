package com.vender98.personalaccount.dagger.modules;

import android.app.Application;
import android.content.Context;

import com.squareup.otto.Bus;
import com.vender98.personalaccount.App;
import com.vender98.personalaccount.events.AndroidBus;
import com.vender98.personalaccount.model.UserProfile;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private App mApp;
    private Context mContext;

    public AppModule(Context context) {
        mContext = context;
    }

    public AppModule(App app) {
        mApp = app;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mApp;
    }

    @Provides
    @Singleton
    public Bus provideBus() {
        return new AndroidBus();
    }

    @Provides
    @Singleton
    public UserProfile provideUserProfile() {
        return new UserProfile(mApp);
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mContext;
    }
}
