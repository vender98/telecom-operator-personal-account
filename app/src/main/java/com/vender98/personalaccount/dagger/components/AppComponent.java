package com.vender98.personalaccount.dagger.components;

import com.vender98.personalaccount.applicationfunctions.splash.activities.SplashActivity;
import com.vender98.personalaccount.dagger.modules.ApiModule;
import com.vender98.personalaccount.dagger.modules.AppModule;
import com.vender98.personalaccount.abstractfragments.BaseFragment;
import com.vender98.personalaccount.applicationfunctions.spendinghistory.fragments.DatePickerFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                AppModule.class,
                ApiModule.class
        })

public interface AppComponent {
    void inject(BaseFragment baseFragment);
    void inject(DatePickerFragment datePickerFragment);
    void inject(SplashActivity splashActivity);
}
