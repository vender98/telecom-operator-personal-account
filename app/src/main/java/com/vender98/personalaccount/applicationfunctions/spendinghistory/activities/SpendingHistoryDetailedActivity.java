package com.vender98.personalaccount.applicationfunctions.spendinghistory.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;

import com.vender98.personalaccount.abstractactivities.CalendarActivity;
import com.vender98.personalaccount.constants.ParamNames;
import com.vender98.personalaccount.applicationfunctions.spendinghistory.fragments.SpendingHistoryDetailedFragment;

public class SpendingHistoryDetailedActivity extends CalendarActivity {

    private static final String TITLE_KEY = "title";

    private long mFrom, mTo, mSpendingItemId;
    private String mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        mTitle = extras.getString(TITLE_KEY);
        mFrom = extras.getLong(ParamNames.FROM_KEY);
        mTo = extras.getLong(ParamNames.TO_KEY);
        mSpendingItemId = extras.getLong(ParamNames.SPENDING_ITEM_ID);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(mTitle);
        }
        initFragment();
    }

    public static Intent newIntent(Context context, String title, long spendingItemId, long from, long to) {
        Intent intent = new Intent(context, SpendingHistoryDetailedActivity.class);
        Bundle b = new Bundle();
        b.putString(TITLE_KEY, title);
        b.putLong(ParamNames.SPENDING_ITEM_ID, spendingItemId);
        b.putLong(ParamNames.FROM_KEY, from);
        b.putLong(ParamNames.TO_KEY, to);
        intent.putExtras(b);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        return SpendingHistoryDetailedFragment.newInstance(mSpendingItemId, mFrom, mTo);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(TITLE_KEY, mTitle);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mTitle = savedInstanceState.getString(TITLE_KEY);
    }
}
