package com.vender98.personalaccount.applicationfunctions.tariffplan.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vender98.personalaccount.R;
import com.vender98.personalaccount.dagger.modules.ApiModule;
import com.vender98.personalaccount.abstractfragments.BaseFragment;
import com.vender98.personalaccount.model.Characteristic;
import com.vender98.personalaccount.model.GeneralTariffPlan;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class TariffPlanActiveFragment extends BaseFragment {

    @BindView(R.id.tariff_plan_active_name_label)
    protected TextView mNameLabel;
    @BindView(R.id.tariff_plan_active_price_label)
    protected TextView mPriceLabel;
    @BindView(R.id.tariff_plan_active_chars)
    protected RecyclerView mCharsRecyclerView;
    @BindView(R.id.tariff_plan_active_content_layout)
    protected ConstraintLayout mContentLayout;

    private CharAdapter mCharAdapter;

    private Unbinder unbinder;

    public static TariffPlanActiveFragment newInstance() {

        Bundle args = new Bundle();

        TariffPlanActiveFragment fragment = new TariffPlanActiveFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tariff_plan_active, container, false);

        unbinder = ButterKnife.bind(this, v);
        mCharsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void loadData() {

        if (!ApiModule.isNetworkAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.check_internet_connection), Toast.LENGTH_LONG).show();
            return;
        }

        showLoader();
        compositeDisposable.add(restAPI.getGeneralTariffPlan(userProfile.getToken(), userProfile.getLineNumber())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getGeneralTariffPlanObserver()));
    }

    private DisposableSingleObserver<GeneralTariffPlan> getGeneralTariffPlanObserver() {
        return new DisposableSingleObserver<GeneralTariffPlan>() {
            @Override
            public void onSuccess(GeneralTariffPlan generalTariffPlan) {
                handleSucceedGeneralTariffPlanObserving(generalTariffPlan);
            }

            @Override
            public void onError(Throwable e) {
                defaultHandleNetworkConnectionError(e);
            }
        };
    }

    private void handleSucceedGeneralTariffPlanObserving(GeneralTariffPlan generalTariffPlan) {
        mContentLayout.setVisibility(View.VISIBLE);
        mNameLabel.setText(generalTariffPlan.getName());
        mPriceLabel.setText(generalTariffPlan.getPrice());

        if (mCharAdapter == null){
            mCharAdapter = new CharAdapter(generalTariffPlan.getCharacteristics());
            mCharsRecyclerView.setAdapter(mCharAdapter);
        } else {
            mCharAdapter.notifyDataSetChanged();
        }

        //Stop loader
        hideLoader();
    }

    class CharHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tariff_plan_active_chars_icon)
        protected ImageView mIcon;
        @BindView(R.id.tariff_plan_active_chars_general_info)
        protected TextView mGeneralInfo;
        @BindView(R.id.tariff_plan_active_chars_description)
        protected TextView mDescription;

        CharHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Characteristic characteristic) {
            mGeneralInfo.setText(characteristic.getGeneralInfo());
            mDescription.setText(characteristic.getDescription());
        }

    }


    private class CharAdapter extends RecyclerView.Adapter<CharHolder> {

        private List<Characteristic> mCharacteristics;

        CharAdapter(List<Characteristic> characteristics) {
            this.mCharacteristics = characteristics;
        }

        @NonNull
        @Override
        public CharHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new CharHolder(layoutInflater.inflate(R.layout.chars_item, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull CharHolder charHolder, int i) {
            Characteristic characteristic = mCharacteristics.get(i);
            charHolder.bind(characteristic);
        }

        @Override
        public int getItemCount() {
            return mCharacteristics.size();
        }
    }


}
