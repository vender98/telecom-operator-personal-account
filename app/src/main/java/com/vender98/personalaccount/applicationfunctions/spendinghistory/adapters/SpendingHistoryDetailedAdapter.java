package com.vender98.personalaccount.applicationfunctions.spendinghistory.adapters;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vender98.personalaccount.R;
import com.vender98.personalaccount.listeners.OnLoadMoreListener;
import com.vender98.personalaccount.model.SpendingItemDetailed;
import com.vender98.personalaccount.utils.Utils;

import java.sql.Timestamp;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SpendingHistoryDetailedAdapter extends RecyclerView.Adapter {

    private final int VIEW_PROG = 0;
    private final int VIEW_ITEM_DATE = 1;
    private final int VIEW_ITEM_CONTENT = 2;

    private List<Object> itemList;

    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private RecyclerView.OnScrollListener onScrollListener;

    public SpendingHistoryDetailedAdapter(List<Object> itemList, RecyclerView recyclerView, int visibleThreshold) {
        this.itemList = itemList;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            this.onScrollListener = new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            };

            recyclerView.addOnScrollListener(onScrollListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (itemList.get(position) instanceof Timestamp) {
            return VIEW_ITEM_DATE;
        } else if (itemList.get(position) instanceof SpendingItemDetailed) {
            return VIEW_ITEM_CONTENT;
        } else {
            return VIEW_PROG;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        if (viewType == VIEW_ITEM_DATE) {
            viewHolder = new SpendingDetailedItemDateHolder(layoutInflater
                    .inflate(R.layout.spending_item_detailed_date, viewGroup, false));
        } else if (viewType == VIEW_ITEM_CONTENT){
            viewHolder = new SpendingDetailedItemContentHolder(layoutInflater
                    .inflate(R.layout.spending_item_detailed_content, viewGroup, false));
        } else {
            viewHolder = new ProgressViewHolder(layoutInflater
                    .inflate(R.layout.progressbar_item, viewGroup, false));
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof SpendingDetailedItemDateHolder) {
            ((SpendingDetailedItemDateHolder) viewHolder).bind((Timestamp) itemList.get(position));
        } else if (viewHolder instanceof SpendingDetailedItemContentHolder) {
            ((SpendingDetailedItemContentHolder) viewHolder).bind((SpendingItemDetailed) itemList.get(position));
        } else {
            ((SpendingHistoryDetailedAdapter.ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return (itemList == null) ? 0 : itemList.size();
    }

    public void activateListener(RecyclerView recyclerView) {
        recyclerView.addOnScrollListener(this.onScrollListener);
        setLoaded();
    }

    public void deactivateListener(RecyclerView recyclerView) {
        new Handler().post(recyclerView::clearOnScrollListeners);
    }

    private void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void startLoad() {
        itemList.add(null);
        new Handler().post(() -> notifyItemInserted(itemList.size() - 1));
    }

    public void stopLoad() {
        itemList.remove(itemList.size() - 1);
        new Handler().post(() -> notifyItemRemoved(itemList.size()));
        setLoaded();
    }

    public void updateData(List<Object> itemList, boolean addMore) {
        if (addMore) {

            //Check that last item of current list and first item of new list
            //have different date. If same date - remove first element of new list
            //in order not to draw unnecessary date header
            if (this.itemList.size() > 0) {
                SpendingItemDetailed lastItem = (SpendingItemDetailed) this.itemList.get(this.itemList.size() - 1);
                if (lastItem.getDate().equals((Timestamp) itemList.get(0))) {
                    itemList.remove(0);
                }
            }
            this.itemList.addAll(itemList);
        } else {
            this.itemList = itemList;
        }
        notifyDataSetChanged();
    }

    class ProgressViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progressBar1)
        protected ProgressBar progressBar;

        ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, itemView);
        }
    }

    class SpendingDetailedItemDateHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.spending_item_detailed_date_label)
        protected TextView mDateLabel;

        SpendingDetailedItemDateHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Timestamp date) {
            mDateLabel.setText(Utils.formatItemDate(date,
                    itemView.getResources().getStringArray(R.array.spending_history_item_month_names)));
        }
    }

    class SpendingDetailedItemContentHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.spending_item_deatailed_details_label)
        protected TextView mDetailsLabel;
        @BindView(R.id.spending_item_deatailed_amount_label)
        protected TextView mAmountLabel;
        @BindView(R.id.spending_item_deatailed_description_label)
        protected TextView mDescriptionLabel;
        @BindView(R.id.spending_item_detailed_time_label)
        protected TextView mTimeLabel;

        SpendingDetailedItemContentHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(SpendingItemDetailed content) {
            mDetailsLabel.setText(content.getDetails());
            mAmountLabel.setText(String.format(itemView.getContext().getString(R.string.amount_label),
                    String.valueOf(content.getAmount())));
            mDescriptionLabel.setText(content.getDescription());
            mTimeLabel.setText(Utils.formatItemTime(content.getDate()));
        }
    }

}