package com.vender98.personalaccount.applicationfunctions.dashboard.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.vender98.personalaccount.R;
import com.vender98.personalaccount.abstractactivities.SingleFragmentActivity;
import com.vender98.personalaccount.applicationfunctions.dashboard.fragments.DashboardFragment;
import com.vender98.personalaccount.listeners.FragmentOnBackPressed;

public class DashboardActivity extends SingleFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initFragment();
    }

    @Override
    protected Fragment createFragment() {
        return new DashboardFragment();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (!(fragment instanceof FragmentOnBackPressed) || !((FragmentOnBackPressed) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }
}
