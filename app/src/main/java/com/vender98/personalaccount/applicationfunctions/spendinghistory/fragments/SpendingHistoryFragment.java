package com.vender98.personalaccount.applicationfunctions.spendinghistory.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.vender98.personalaccount.R;
import com.vender98.personalaccount.applicationfunctions.spendinghistory.activities.SpendingHistoryDetailedActivity;
import com.vender98.personalaccount.constants.Constants;
import com.vender98.personalaccount.constants.ParamNames;
import com.vender98.personalaccount.dagger.modules.ApiModule;
import com.vender98.personalaccount.events.DatePickerDateSetEvent;
import com.vender98.personalaccount.model.SpendingItem;
import com.vender98.personalaccount.utils.Utils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import lecho.lib.hellocharts.view.PieChartView;

public class SpendingHistoryFragment extends SpendingHistoryAbstractFragment {

    @BindView(R.id.spending_history_recycler_view)
    protected RecyclerView mSpendingHistoryRecyclerView;
    @BindView(R.id.spending_history_content_layout)
    protected ConstraintLayout mContentLayout;
    @BindView(R.id.spending_history_period_label)
    protected TextView mPeriodLabel;
    @BindView(R.id.spending_history_chart)
    protected PieChartView mSpendingHistoryChart;

    private long mFrom, mTo;
    private SpendingItemAdapter mSpendingItemAdapter;
    private Unbinder unbinder;

    public static SpendingHistoryFragment newInstance() {

        Bundle args = new Bundle();

        SpendingHistoryFragment fragment = new SpendingHistoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_spending_history, container, false);

        if (savedInstanceState != null) {
            mFrom = savedInstanceState.getLong(ParamNames.FROM_KEY);
            mTo = savedInstanceState.getLong(ParamNames.TO_KEY);
        }

        unbinder = ButterKnife.bind(this, v);
        mSpendingHistoryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mSpendingHistoryRecyclerView.setHasFixedSize(true);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateDataInTheLastMonth();
    }

    private DisposableSingleObserver<List<SpendingItem>> getSpendingHistoryObserver(long from, long to) {
        return new DisposableSingleObserver<List<SpendingItem>>() {
            @Override
            public void onSuccess(List<SpendingItem> spendingItems) {
                handleSucceedSpendingHistoryObserving(spendingItems, from, to);
            }

            @Override
            public void onError(Throwable e) {
                defaultHandleNetworkConnectionError(e);
            }
        };
    }

    private void handleSucceedSpendingHistoryObserving(List<SpendingItem> spendingItems, long from, long to) {
        spendingItems.sort((i1, i2) -> i2.getAmount() - i1.getAmount());
        List<Integer> colors = Arrays.stream(Constants.CHART_COLORS)
                .boxed()
                .collect(Collectors.toList());
        initSpendingChart(mSpendingHistoryChart, spendingItems, colors, getString(R.string.amount_label));

        if (mSpendingItemAdapter == null){
            mSpendingItemAdapter = new SpendingItemAdapter(spendingItems, colors);
            mSpendingHistoryRecyclerView.setAdapter(mSpendingItemAdapter);
        } else {
            mSpendingItemAdapter.updateDate(spendingItems, colors);
            mSpendingItemAdapter.notifyDataSetChanged();
        }

        mFrom = from;
        mTo = to;

        mContentLayout.setVisibility(View.VISIBLE);
        //Stop loader
        hideLoader();
    }

    private void initSpendingChart(PieChartView view, List<SpendingItem> spendingItems, List<Integer> colors, String label) {
        List<Integer> values = spendingItems
                .stream()
                .map(SpendingItem::getAmount)
                .collect(Collectors.toList());
        if (values.size() > colors.size()) {
            values = values.subList(0, colors.size() - 1);
        }

        Utils.initChart(view, values, colors, String.format(label, values
                .stream()
                .reduce((v1, v2) -> v1 + v2)
                .orElse(0)));
    }

    @Override
    protected void updateData(long from, long to) {
        if (!ApiModule.isNetworkAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.check_internet_connection), Toast.LENGTH_LONG).show();
            return;
        }

        showLoader();
        mPeriodLabel.setText(String.format(getString(R.string.spending_period_label),
                Utils.formatGeneralDate(new Date(from)), Utils.formatGeneralDate(new Date(to))));

        compositeDisposable.add(restAPI.getSpendingHistory(userProfile.getToken(), userProfile.getLineNumber(), from, to)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getSpendingHistoryObserver(from, to)));
    }

    @Subscribe
    @Override
    public void onDatePickerDateSet(DatePickerDateSetEvent event) {
        super.onDatePickerDateSet(event);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putLong(ParamNames.FROM_KEY, mFrom);
        outState.putLong(ParamNames.TO_KEY, mTo);
        super.onSaveInstanceState(outState);
    }

    class SpendingItemHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.spending_item_name_label)
        protected TextView mName;
        @BindView(R.id.spending_item_amount_label)
        protected TextView mAmount;
        @BindView(R.id.spending_item_color_container)
        protected FrameLayout mColorContainer;

        SpendingItemHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(SpendingItem spendingItem, Integer color) {
            mName.setText(spendingItem.getName());
            mAmount.setText(String.format(getString(R.string.amount_label), String.valueOf(spendingItem.getAmount())));
            mColorContainer.setBackgroundColor(color);
        }
    }

    private class SpendingItemAdapter extends RecyclerView.Adapter<SpendingItemHolder> {

        private List<SpendingItem> mSpendingItems;
        private List<Integer> mColors;

        SpendingItemAdapter(List<SpendingItem> spendingItems, List<Integer> colors) {
            updateDate(spendingItems, colors);
        }

        @NonNull
        @Override
        public SpendingItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new SpendingItemHolder(layoutInflater.inflate(R.layout.spending_item, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull SpendingItemHolder spendingItemHolder, int i) {
            spendingItemHolder.bind(mSpendingItems.get(i), i < mColors.size() ? mColors.get(i) : Color.WHITE);
            spendingItemHolder.itemView.setOnClickListener(v -> {
                SpendingItem spendingItem = mSpendingItems.get(i);
                Intent intent = SpendingHistoryDetailedActivity.newIntent(getContext(),
                        spendingItem.getName(), spendingItem.getItemTypeId(), mFrom, mTo);
                startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return (mSpendingItemAdapter == null) ? 0 : mSpendingItems.size();
        }

        public void updateDate(List<SpendingItem> spendingItems, List<Integer> colors) {
            mSpendingItems = spendingItems;
            mColors = colors;
        }
    }

}
