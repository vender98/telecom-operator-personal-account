package com.vender98.personalaccount.applicationfunctions.spendinghistory.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;

import com.squareup.otto.Bus;
import com.vender98.personalaccount.App;
import com.vender98.personalaccount.constants.Constants;
import com.vender98.personalaccount.constants.DatePickerOpenTime;
import com.vender98.personalaccount.events.DatePickerDateSetEvent;

import java.util.Calendar;

import javax.inject.Inject;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private static final String OPEN_TIME_KEY = "openTime";

    private DatePickerOpenTime openTime;

    @Inject
    protected Bus androidBus;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        App.from(context).appComponent().inject(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        androidBus.register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        androidBus.unregister(this);
    }

    public static DatePickerFragment newInstance(DatePickerOpenTime openTime) {

        Bundle args = new Bundle();
        args.putSerializable(OPEN_TIME_KEY, openTime);
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.openTime = (DatePickerOpenTime) getArguments().getSerializable(OPEN_TIME_KEY);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        c.add(Calendar.MONTH, Math.negateExact(Constants.SPENDING_HISTORY_AVAILABLE_PERIOD_MONTHS));
        dialog.getDatePicker().setMinDate(c.getTimeInMillis());
        return dialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        
        long from, to;
        if (openTime == DatePickerOpenTime.FROM) {
            from = calendar.getTimeInMillis();
            to = 0;
        } else {
            from = 0;
            to = calendar.getTimeInMillis();
        }
        
        DatePickerDateSetEvent event = new DatePickerDateSetEvent(from, to, openTime);
        androidBus.post(event);
    }
}
