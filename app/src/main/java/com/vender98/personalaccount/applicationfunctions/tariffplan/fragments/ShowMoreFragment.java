package com.vender98.personalaccount.applicationfunctions.tariffplan.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.vender98.personalaccount.R;
import com.vender98.personalaccount.constants.Constants;
import com.vender98.personalaccount.dagger.modules.ApiModule;
import com.vender98.personalaccount.events.ShowMorePlansEvent;
import com.vender98.personalaccount.abstractfragments.BaseFragment;
import com.vender98.personalaccount.model.GeneralTariffPlan;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ShowMoreFragment extends BaseFragment {

    private static final String RANGE_KEY = "range";

    @BindView(R.id.show_more_button)
    protected ImageView mShowMoreButton;

    private int mRange;

    private Unbinder unbinder;

    public static ShowMoreFragment newInstance(int range) {

        Bundle args = new Bundle();
        args.putInt(RANGE_KEY, range);
        ShowMoreFragment fragment = new ShowMoreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRange = getArguments().getInt(RANGE_KEY);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.show_more, container, false);

        unbinder = ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.show_more_button)
    protected void onShowMoreButtonClick() {
        if (!ApiModule.isNetworkAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.check_internet_connection), Toast.LENGTH_LONG).show();
            return;
        }

        showLoader();
        compositeDisposable.add(Single.zip(
                restAPI.getAvailableTariffPlans(userProfile.getToken(), userProfile.getLineNumber(), mRange),
                restAPI.hasAvailableTariffPlans(userProfile.getToken(), userProfile.getLineNumber(), mRange + 1),
                (tariffPlans, hasAvailableTariffPlans) -> {
                    Map<String, Object> modelMap = new HashMap<>();
                    modelMap.put(Constants.TARIFF_PLANS_KEY, tariffPlans);
                    modelMap.put(Constants.HAS_AVAILABLE_TARIFF_PLANS_FLAG_KEY,
                            hasAvailableTariffPlans.get(Constants.HAS_AVAILABLE_TARIFF_PLANS_FLAG_KEY));
                    return modelMap;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getAvailableTariffPlansObserver()));
    }

    private DisposableSingleObserver<Map<String, Object>> getAvailableTariffPlansObserver() {
        return new DisposableSingleObserver<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> modelMap) {
                handleSucceedAvailableTariffPlansObserving(modelMap);
            }

            @Override
            public void onError(Throwable e) {
                defaultHandleNetworkConnectionError(e);
            }
        };
    }

    private void handleSucceedAvailableTariffPlansObserving(Map<String, Object> modelMap) {
        List<GeneralTariffPlan> tariffPlans = (List<GeneralTariffPlan>) modelMap.get(Constants.TARIFF_PLANS_KEY);
        boolean hasAvailableTariffPlans = (boolean) modelMap.get(Constants.HAS_AVAILABLE_TARIFF_PLANS_FLAG_KEY);

        //Stop loader
        hideLoader();

        androidBus.post(new ShowMorePlansEvent(tariffPlans, hasAvailableTariffPlans));
    }
}
