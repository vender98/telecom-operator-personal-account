package com.vender98.personalaccount.applicationfunctions.tariffplan.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.vender98.personalaccount.R;
import com.vender98.personalaccount.constants.Constants;
import com.vender98.personalaccount.dagger.modules.ApiModule;
import com.vender98.personalaccount.events.ShowMorePlansEvent;
import com.vender98.personalaccount.abstractfragments.BaseFragment;
import com.vender98.personalaccount.model.GeneralTariffPlan;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class TariffPlanAvailableFragment extends BaseFragment {

    @BindView(R.id.tariff_plan_available_tariff_plans)
    protected ViewPager mTariffPlansViewPager;
    @BindView(R.id.tariff_plan_available_tariff_pointer_label)
    protected TextView mTariffPlanPointer;
    @BindView(R.id.tariff_plan_available_content_layout)
    protected ConstraintLayout mContentLayout;

    private TariffPlansViewPagerAdapter mTariffPlansViewPagerAdapter;

    private Unbinder unbinder;

    public static TariffPlanAvailableFragment newInstance() {

        Bundle args = new Bundle();

        TariffPlanAvailableFragment fragment = new TariffPlanAvailableFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tariff_plan_available, container, false);

        unbinder = ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void loadData() {

        if (!ApiModule.isNetworkAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.check_internet_connection), Toast.LENGTH_LONG).show();
            return;
        }

        showLoader();
        compositeDisposable.add(Single.zip(
                restAPI.getAvailableTariffPlans(userProfile.getToken(), userProfile.getLineNumber(), 1),
                restAPI.hasAvailableTariffPlans(userProfile.getToken(), userProfile.getLineNumber(), 2),
                (tariffPlans, hasAvailableTariffPlans) -> {
                    Map<String, Object> modelMap = new HashMap<>();
                    modelMap.put(Constants.TARIFF_PLANS_KEY, tariffPlans);
                    modelMap.put(Constants.HAS_AVAILABLE_TARIFF_PLANS_FLAG_KEY,
                            hasAvailableTariffPlans.get(Constants.HAS_AVAILABLE_TARIFF_PLANS_FLAG_KEY));
                    return modelMap;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getAvailableTariffPlansObserver()));
    }

    private DisposableSingleObserver<Map<String, Object>> getAvailableTariffPlansObserver() {
        return new DisposableSingleObserver<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> modelMap) {
                handleSucceedAvailableTariffPlansObserving(modelMap);
            }

            @Override
            public void onError(Throwable e) {
                defaultHandleNetworkConnectionError(e);
            }
        };
    }

    private void handleSucceedAvailableTariffPlansObserving(Map<String, Object> modelMap) {
        List<GeneralTariffPlan> tariffPlans = (List<GeneralTariffPlan>) modelMap.get(Constants.TARIFF_PLANS_KEY);
        boolean hasAvailableTariffPlans = (boolean) modelMap.get(Constants.HAS_AVAILABLE_TARIFF_PLANS_FLAG_KEY);

        if (mTariffPlansViewPagerAdapter == null){
            mTariffPlansViewPagerAdapter = new TariffPlansViewPagerAdapter(getChildFragmentManager(), tariffPlans,
                    hasAvailableTariffPlans);
            mTariffPlansViewPager.setAdapter(mTariffPlansViewPagerAdapter);
        } else {
            mTariffPlansViewPagerAdapter.notifyDataSetChanged();
        }
        mTariffPlansViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                updatePointerLabel(i + 1, tariffPlans.size());
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        updatePointerLabel(1, tariffPlans.size());
        mContentLayout.setVisibility(View.VISIBLE);

        //Stop loader
        hideLoader();
    }

    private void updatePointerLabel (int currentPosition, int size) {
        String text = currentPosition > size ?
                null :
                String.format(getString(R.string.tariff_plan_pointer), currentPosition, size);
        mTariffPlanPointer.setText(text);
    }

    @Subscribe
    public void onShowMorePlansEvent(ShowMorePlansEvent event) {
        int currentPlansCount = mTariffPlansViewPagerAdapter.getCount() - 1;
        boolean hasAvailableTariffPlans = event.hasAvailableTariffPlans();
        mTariffPlansViewPagerAdapter.addTariffPlans(event.getTariffPlans(), hasAvailableTariffPlans);
        mTariffPlansViewPagerAdapter.notifyDataSetChanged();
        mTariffPlansViewPager.setCurrentItem(currentPlansCount);
        updatePointerLabel(currentPlansCount + 1,
                hasAvailableTariffPlans ?
                mTariffPlansViewPagerAdapter.getCount() - 1 :
                mTariffPlansViewPagerAdapter.getCount());

    }

    private class TariffPlansViewPagerAdapter extends FragmentStatePagerAdapter {

        private List<GeneralTariffPlan> mTariffPlans;
        private boolean mHasAvailableTariffPlans;

        TariffPlansViewPagerAdapter(FragmentManager fm, List<GeneralTariffPlan> tariffPlans, boolean hasAvailableTariffPlans) {
            super(fm);
            mTariffPlans = tariffPlans;
            mHasAvailableTariffPlans = hasAvailableTariffPlans;
        }

        @Override
        public Fragment getItem(int i) {
            return mHasAvailableTariffPlans && i == mTariffPlans.size() ?
                    ShowMoreFragment.newInstance(mTariffPlans.size() / Constants.DEFAULT_RANGE_SIZE + 1) :
                    TariffPlanFragment.newInstance(mTariffPlans.get(i));
        }

        @Override
        public int getCount() {
            return mHasAvailableTariffPlans ?
                    mTariffPlans.size() + 1 :
                    mTariffPlans.size();
        }

        public void addTariffPlans(List<GeneralTariffPlan> tariffPlans, boolean hasAvailableTariffPlans) {
            mTariffPlans.addAll(tariffPlans);
            mHasAvailableTariffPlans = hasAvailableTariffPlans;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            if (object instanceof ShowMoreFragment) {
                return POSITION_NONE;
            }
            return super.getItemPosition(object);
        }
    }
}
