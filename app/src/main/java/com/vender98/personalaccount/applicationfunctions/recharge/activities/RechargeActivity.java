package com.vender98.personalaccount.applicationfunctions.recharge.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.vender98.personalaccount.R;
import com.vender98.personalaccount.abstractactivities.SingleFragmentNavigateUpActivity;
import com.vender98.personalaccount.applicationfunctions.recharge.fragments.RechargeFragment;

public class RechargeActivity extends SingleFragmentNavigateUpActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initFragment();
    }

    @Override
    protected Fragment createFragment() {
        return new RechargeFragment();
    }

    @Override
    protected int getActivityTitle() {
        return R.string.recharge_activity_title;
    }
}
