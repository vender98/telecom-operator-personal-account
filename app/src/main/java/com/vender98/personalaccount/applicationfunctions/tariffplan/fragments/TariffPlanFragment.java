package com.vender98.personalaccount.applicationfunctions.tariffplan.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vender98.personalaccount.R;
import com.vender98.personalaccount.constants.Constants;
import com.vender98.personalaccount.dagger.modules.ApiModule;
import com.vender98.personalaccount.events.ChangeTariffPlanEvent;
import com.vender98.personalaccount.abstractfragments.BaseFragment;
import com.vender98.personalaccount.model.CustomerAccount;
import com.vender98.personalaccount.model.CustomerTariffPlan;
import com.vender98.personalaccount.model.GeneralTariffPlan;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class TariffPlanFragment extends BaseFragment {

    public static final String TARIFF_PLAN_KEY = "tariff_plan";

    @BindView(R.id.tariff_plan_available_name_label)
    protected TextView mNameLabel;
    @BindView(R.id.tariff_plan_available_sms_icon)
    protected ImageView mSmsIcon;
    @BindView(R.id.tariff_plan_available_sms_label)
    protected TextView mSmsLabel;
    @BindView(R.id.tariff_plan_available_internet_icon)
    protected ImageView mInternetIcon;
    @BindView(R.id.tariff_plan_available_internet_label)
    protected TextView mInternetLabel;
    @BindView(R.id.tariff_plan_available_minutes_icon)
    protected ImageView mMinutesIcon;
    @BindView(R.id.tariff_plan_available_minutes_label)
    protected TextView mMinutesLabel;
    @BindView(R.id.tariff_plan_available_price_label)
    protected TextView mPriceLabel;
    @BindView(R.id.tariff_plan_available_choose_button)
    protected Button mChooseButton;

    private GeneralTariffPlan mTariffPlan;

    private Unbinder unbinder;

    public static TariffPlanFragment newInstance(GeneralTariffPlan tariffPlan) {

        Bundle args = new Bundle();
        args.putSerializable(TARIFF_PLAN_KEY, tariffPlan);
        TariffPlanFragment fragment = new TariffPlanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTariffPlan = (GeneralTariffPlan) getArguments().getSerializable(TARIFF_PLAN_KEY);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tariff_plan, container, false);

        unbinder = ButterKnife.bind(this, v);

        mNameLabel.setText(mTariffPlan.getName());
        mPriceLabel.setText(mTariffPlan.getPrice());
        mTariffPlan.getCharacteristics().forEach(characteristic -> {
            switch (characteristic.getType()) {
                case Constants.MINUTES:
                    mMinutesLabel.setText(characteristic.getGeneralInfo());
                    break;
                case Constants.SMS:
                    mSmsLabel.setText(characteristic.getGeneralInfo());
                    break;
                case Constants.INTERNET:
                    mInternetLabel.setText(characteristic.getGeneralInfo());
                    break;
            }
        });

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.tariff_plan_available_choose_button)
    public void onChooseButtonClick (View v) {
        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.change_plan_title, mTariffPlan.getName()))
                .setMessage(getString(R.string.change_plan_message))
                .setPositiveButton( getString(R.string.change_button), (dialog1, which) -> {
                    if (!ApiModule.isNetworkAvailable(getContext())){
                        Toast.makeText(getContext(), getString(R.string.check_internet_connection), Toast.LENGTH_LONG).show();
                        return;
                    }

                    showLoader();
                    compositeDisposable.add(Single.zip(
                            restAPI.changeTariffPlan(userProfile.getToken(), userProfile.getLineNumber(), mTariffPlan.getId()),
                            restAPI.getCustomerAccount(userProfile.getToken(), userProfile.getLineNumber()),
                            (customerTariffPlan, customerAccount) -> {
                                Map<String, Object> modelMap = new HashMap<>();
                                modelMap.put(Constants.CUSTOMER_ACCOUNT_KEY, customerAccount);
                                modelMap.put(Constants.CUSTOMER_TARIFF_PLAN_KEY, customerTariffPlan);
                                return modelMap;
                            })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(getChangePlanObserver()));

                })
                .setNegativeButton(getString(R.string.cancel_buton), (dialog1, which) -> dialog1.cancel())
                .create();
        dialog.setOnShowListener(dialogInterface -> {
            Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            positiveButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            positiveButton.setTextColor(Color.WHITE);
        });
        dialog.show();
    }

    private DisposableSingleObserver<Map<String, Object>> getChangePlanObserver() {
        return new DisposableSingleObserver<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> modelMap) {
                handleSucceedChangePlanObserving(modelMap);
            }

            @Override
            public void onError(Throwable e) {
                if (e instanceof HttpException) {
                    int code = ((HttpException) e).code();
                    switch (code){
                        case HttpURLConnection.HTTP_BAD_REQUEST:
                            handleApiError(e, getString(R.string.change_plan_insufficient_funds));
                            break;
                        default:
                            defaultHandleNetworkConnectionError(e);
                            break;

                    }
                }
            }
        };
    }

    private void handleSucceedChangePlanObserving(Map<String, Object> modelMap) {
        CustomerTariffPlan customerTariffPlan = (CustomerTariffPlan) modelMap.get(Constants.CUSTOMER_TARIFF_PLAN_KEY);
        CustomerAccount customerAccount = (CustomerAccount) modelMap.get(Constants.CUSTOMER_ACCOUNT_KEY);
        androidBus.post(new ChangeTariffPlanEvent(customerTariffPlan, customerAccount));

        //Stop loader
        hideLoader();
        getActivity().finish();
    }
}
