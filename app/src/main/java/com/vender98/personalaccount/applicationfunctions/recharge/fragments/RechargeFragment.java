package com.vender98.personalaccount.applicationfunctions.recharge.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;

import com.vender98.personalaccount.R;
import com.vender98.personalaccount.constants.Constants;
import com.vender98.personalaccount.events.AmountChangedEvent;
import com.vender98.personalaccount.abstractfragments.BaseFragment;
import com.vender98.personalaccount.utils.Utils;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class RechargeFragment extends BaseFragment {

    @BindView(R.id.amountInput)
    protected EditText mAmountInput;
    @BindView(R.id.payButton)
    protected Button payButton;
    @BindView(R.id.userPaymentDataInputLayout)
    protected FrameLayout mDataInputLayout;

    private Unbinder unbinder;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_recharge, container, false);

        unbinder = ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.payButton)
    public void onPayButtonClick(View v) {
        Integer amount;
        try {
            amount = Integer.valueOf(mAmountInput.getText().toString());
        } catch (NumberFormatException e) {
            mAmountInput.setError(getString(R.string.input_should_not_be_empty_error));
            e.printStackTrace();
            return;
        }

        showLoader();
        compositeDisposable.add(restAPI.rechargeCustomerAccount(userProfile.getToken(), userProfile.getLineNumber(), amount)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(rechargeCustomerAccountObserver()));
    }

    @OnClick(R.id.amountInput)
    protected void onAmountInputClick() {
        mAmountInput.setError(null);
    }

    @OnEditorAction(R.id.amountInput)
    protected boolean onEditorAction(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
            Utils.hideSoftKeyboard(getActivity());
        }
        return true;
    }

    @OnClick({R.id.cardRadioButton, R.id.walletRadioButton, R.id.googlePayRadioButton})
    protected void onPaymentMethodRadioButtonClick (RadioButton button) {
        boolean checked = button.isChecked();
        switch (button.getId()) {
            case R.id.cardRadioButton:
                if (checked) {
                    mDataInputLayout.findViewById(R.id.cardForm).setVisibility(View.VISIBLE);
                }
                break;
            default:
                mDataInputLayout.findViewById(R.id.cardForm).setVisibility(View.GONE);

        }
    }


    private DisposableSingleObserver<Map<String, Integer>> rechargeCustomerAccountObserver() {
        return new DisposableSingleObserver<Map<String, Integer>>() {
            @Override
            public void onSuccess(Map<String, Integer> stringObjectMap) {
                handleSucceedCustomerAccountObserving(stringObjectMap);
            }

            @Override
            public void onError(Throwable e) {
                defaultHandleNetworkConnectionError(e);
            }
        };
    }

    private void handleSucceedCustomerAccountObserving(Map<String, Integer> stringObjectMap) {
        Integer amount = stringObjectMap.get(Constants.BALANCE_KEY);
        androidBus.post(new AmountChangedEvent(amount));

        //Stop loader
        hideLoader();
        getActivity().finish();
    }
}
