package com.vender98.personalaccount.applicationfunctions.authentication.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy;
import com.vender98.personalaccount.R;
import com.vender98.personalaccount.abstractfragments.BaseFragment;
import com.vender98.personalaccount.applicationfunctions.dashboard.activities.DashboardActivity;
import com.vender98.personalaccount.constants.Constants;
import com.vender98.personalaccount.utils.Utils;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class AuthenticationFragment extends BaseFragment {

    @BindView(R.id.authentication_phone_container_layout)
    protected ConstraintLayout mPhoneLayout;
    @BindView(R.id.authentication_phone_number)
    protected EditText mPhoneNumberInput;
    @BindView(R.id.authentication_sms_button)
    protected Button mSmsButton;

    @BindView(R.id.authentication_sms_container_layout)
    protected ConstraintLayout mSmsLayout;
    @BindView(R.id.authentication_code_input)
    protected EditText mCodeInput;
    @BindView(R.id.authentication_timer_label)
    protected TextView mTimerLabel;
    @BindView(R.id.authentication_login_button)
    protected Button mLoginButton;
    @BindView(R.id.authentication_resend_button)
    protected Button mResendButton;

    private Unbinder unbinder;
    private String token;
    private long lineNumber;
    private CountDownTimer mSmsTimer = getSmsTimer();

    public static AuthenticationFragment newInstance() {

        Bundle args = new Bundle();

        AuthenticationFragment fragment = new AuthenticationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_authentication, container, false);

        unbinder = ButterKnife.bind(this, v);
        setupPhoneInputMask();

        return v;
    }

    private void setupPhoneInputMask() {
        final List<String> affineFormats = new ArrayList<>();
        affineFormats.add("8 ([000]) [000]-[00]-[00]");

        final MaskedTextChangedListener listener = MaskedTextChangedListener.Companion.installOn(
                mPhoneNumberInput,
                "+7 ([000]) [000]-[00]-[00]",
                affineFormats,
                AffinityCalculationStrategy.PREFIX,
                null
        );

        mPhoneNumberInput.setHint(listener.placeholder());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnEditorAction(R.id.authentication_phone_number)
    protected boolean onPhoneNumberInputEditorAction(int actionId) {
        if (actionId == EditorInfo.IME_ACTION_DONE
                || actionId == EditorInfo.IME_ACTION_NEXT
                || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
            Utils.hideSoftKeyboard(getActivity());
        }
        return true;
    }

    @OnClick(R.id.authentication_sms_button)
    protected void onSmsButtonClick(View v) {
        String phoneNumber = mPhoneNumberInput.getText().toString();
        if (phoneNumber.isEmpty()) {
            mPhoneNumberInput.setError(getString(R.string.input_should_not_be_empty_error));
        } else if (!Utils.isLineNumberValid(phoneNumber)) {
            mPhoneNumberInput.setError(getString(R.string.input_line_number_incorrect_error));
        } else {
            long digitizedLineNumber = Utils.formatLineNumber(phoneNumber);

            showLoader();
            compositeDisposable.add(restAPI.sendSmsCode(digitizedLineNumber)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(sendSmsCodeObserver(digitizedLineNumber)));
            mPhoneNumberInput.setText("");
        }
    }

    private DisposableSingleObserver<String> sendSmsCodeObserver(long lineNumber) {
        return new DisposableSingleObserver<String>() {
            @Override
            public void onSuccess(String s) {
                onSucceedSendSmsCodeObserving(s, lineNumber);
            }

            @Override
            public void onError(Throwable e) {
                onErrorSendSmsCodeObserving(e, lineNumber);
            }
        };
    }

    private void onSucceedSendSmsCodeObserving(String token, long lineNumber) {
        this.token = token;
        this.lineNumber = lineNumber;
        mPhoneLayout.setVisibility(View.GONE);
        mSmsLayout.setVisibility(View.VISIBLE);
        startSmsTimer();

        //Hide loader
        hideLoader();
    }

    private void startSmsTimer() {
        mTimerLabel.setVisibility(View.VISIBLE);
        mSmsTimer.start();
    }

    private void onErrorSendSmsCodeObserving(Throwable e, long lineNumber) {
        if (e instanceof HttpException) {
            int code = ((HttpException) e).code();
            if (code == Constants.HTTP_UNPROCESSABLE_ENTITY) {
                Toast.makeText(getContext(), String.format(getString(R.string.authentication_user_not_found),
                        Utils.formatLineNumber(lineNumber)),
                        Toast.LENGTH_LONG).show();
            }
            hideLoader();
            return;
        }
        defaultHandleNetworkConnectionError(e);
    }

    @OnClick(R.id.authentication_login_button)
    protected void onLoginButtonClick(View v) {
        String code = mCodeInput.getText().toString();
        if (code.isEmpty()) {
            mCodeInput.setError(getString(R.string.input_should_not_be_empty_error));
        } else if (!Utils.isSmsCodeValid(code)) {
            mCodeInput.setError(getString(R.string.input_sms_code_incorrect_error));
            mCodeInput.setText("");
        } else {
            showLoader();
            compositeDisposable.add(restAPI.verifySmsCode(this.token, code)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(verifySmsCodeObserver()));
            mCodeInput.setText("");
        }
    }

    private DisposableCompletableObserver verifySmsCodeObserver() {
        return new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                onSucceedVerifySmsCodeObserving();
            }

            @Override
            public void onError(Throwable e) {
                onErrorVerifySmsCodeObserving(e);
            }
        };
    }

    private void onSucceedVerifySmsCodeObserving() {
        userProfile.setToken(this.token);
        userProfile.setLineNumber(lineNumber);
        mSmsTimer.cancel();

        Intent intent = new Intent(getActivity(), DashboardActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void onErrorVerifySmsCodeObserving(Throwable e) {
        if (e instanceof HttpException) {
            int code = ((HttpException) e).code();
            switch (code){
                case HttpURLConnection.HTTP_NOT_FOUND:
                    Toast.makeText(getContext(), getString(R.string.authentication_token_invalid), Toast.LENGTH_LONG).show();
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    Toast.makeText(getContext(), getString(R.string.authentication_too_foten_verify), Toast.LENGTH_LONG).show();
                    mLoginButton.setEnabled(false);
                    break;
                case Constants.HTTP_UNPROCESSABLE_ENTITY:
                    Toast.makeText(getContext(), getString(R.string.authentication_code_invalid), Toast.LENGTH_LONG).show();
                    break;
            }
            hideLoader();
            return;
        }
        defaultHandleNetworkConnectionError(e);
    }

    @OnClick(R.id.authentication_resend_button)
    protected void onResendButtonClick() {
        showLoader();
        compositeDisposable.add(restAPI.resendSmsCode(this.token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(resendSmsCodeObserver()));
        mPhoneNumberInput.setText("");

    }

    private DisposableCompletableObserver resendSmsCodeObserver() {
        return new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                onSucceedResendSmsCodeObserving();
            }

            @Override
            public void onError(Throwable e) {
                onErrorResendSmsCodeObserving(e);
            }
        };
    }

    private void onSucceedResendSmsCodeObserving() {
        mLoginButton.setEnabled(true);
        mResendButton.setVisibility(View.INVISIBLE);
        startSmsTimer();

        //Hide loader
        hideLoader();
    }

    private void onErrorResendSmsCodeObserving(Throwable e) {
        if (e instanceof HttpException) {
            int code = ((HttpException) e).code();
            if (code == HttpURLConnection.HTTP_NOT_FOUND) {
                Toast.makeText(getContext(), getString(R.string.authentication_token_invalid), Toast.LENGTH_LONG).show();
                hideLoader();
                return;
            }
        }
        defaultHandleNetworkConnectionError(e);
    }

    private CountDownTimer getSmsTimer() {
        return new CountDownTimer(Constants.SMS_CONFIRMATION_TIMEOUT, Constants.SMS_CONFIRMATION_DELAY) {

            @Override
            public void onTick(long millisUntilFinished) {
                mTimerLabel.setText(String.format(getString(R.string.authentication_timer_label), millisUntilFinished / Constants.SMS_CONFIRMATION_DELAY));
            }

            @Override
            public void onFinish() {
                mTimerLabel.setVisibility(View.INVISIBLE);
                mResendButton.setVisibility(View.VISIBLE);
                mLoginButton.setEnabled(false);
            }
        };
    }
}