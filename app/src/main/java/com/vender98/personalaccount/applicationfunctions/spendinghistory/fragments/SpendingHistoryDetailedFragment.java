package com.vender98.personalaccount.applicationfunctions.spendinghistory.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.vender98.personalaccount.R;
import com.vender98.personalaccount.applicationfunctions.spendinghistory.adapters.SpendingHistoryDetailedAdapter;
import com.vender98.personalaccount.constants.Constants;
import com.vender98.personalaccount.constants.ParamNames;
import com.vender98.personalaccount.dagger.modules.ApiModule;
import com.vender98.personalaccount.events.DatePickerDateSetEvent;
import com.vender98.personalaccount.model.SpendingItemDetailed;
import com.vender98.personalaccount.utils.Utils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static java.util.stream.Collectors.toList;

public class SpendingHistoryDetailedFragment extends SpendingHistoryAbstractFragment {

    @BindView(R.id.spending_history_detailed_content_layout)
    protected ConstraintLayout mContentLayout;
    @BindView(R.id.spending_history_detailed_period_label)
    protected TextView mPeriodLabel;
    @BindView(R.id.spending_history_detailed_recycler_view)
    protected RecyclerView mSpendingHistoryDetailedRecyclerView;

    private long mFrom, mTo, mSpendingItemId;

    private boolean mHasAvailableSpendingItemHistory;
    private int mRange;

    private SpendingHistoryDetailedAdapter mSpendingHistoryDetailedAdapter;
    private Unbinder unbinder;

    public static SpendingHistoryDetailedFragment newInstance(long spendingItemId, long from, long to) {

        Bundle args = new Bundle();
        args.putLong(ParamNames.SPENDING_ITEM_ID, spendingItemId);
        args.putLong(ParamNames.FROM_KEY, from);
        args.putLong(ParamNames.TO_KEY, to);
        SpendingHistoryDetailedFragment fragment = new SpendingHistoryDetailedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mFrom = args.getLong(ParamNames.FROM_KEY);
        mTo = args.getLong(ParamNames.TO_KEY);
        mSpendingItemId = args.getLong(ParamNames.SPENDING_ITEM_ID);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_spending_history_detailed, container, false);

        if (savedInstanceState != null) {
            mFrom = savedInstanceState.getLong(ParamNames.FROM_KEY);
            mTo = savedInstanceState.getLong(ParamNames.TO_KEY);
            mSpendingItemId = savedInstanceState.getLong(ParamNames.SPENDING_ITEM_ID);
        }
        unbinder = ButterKnife.bind(this, v);
        mSpendingHistoryDetailedRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateData(mFrom, mTo);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putLong(ParamNames.FROM_KEY, mFrom);
        outState.putLong(ParamNames.TO_KEY, mTo);
        outState.putLong(ParamNames.SPENDING_ITEM_ID, mSpendingItemId);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void updateData(long from, long to) {
        if (!ApiModule.isNetworkAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.check_internet_connection), Toast.LENGTH_LONG).show();
            return;
        }

        showLoader();
        mPeriodLabel.setText(String.format(getString(R.string.spending_period_label),
                Utils.formatGeneralDate(new Date(from)), Utils.formatGeneralDate(new Date(to))));

        compositeDisposable.add(Single.zip(
                restAPI.getItemSpendingHistory(userProfile.getToken(), userProfile.getLineNumber(), mSpendingItemId, from, to, 1),
                restAPI.hasAvailableSpendingItemHistory(userProfile.getToken(), userProfile.getLineNumber(), mSpendingItemId, from, to, 2),
                (spendingItemDetailedList, hasAvailableSpendingItemHistory) -> {
                    Map<String, Object> modelMap = new HashMap<>();
                    modelMap.put(Constants.SPENDING_DETAILED_ITEMS, spendingItemDetailedList);
                    modelMap.put(Constants.HAS_AVAILABLE_SPENDING_ITEM_HISTORY_FLAG_KEY,
                            hasAvailableSpendingItemHistory.get(Constants.HAS_AVAILABLE_SPENDING_ITEM_HISTORY_FLAG_KEY));
                    return modelMap;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getItemSpendingHistoryObserver(from, to)));
    }

    private void loadMoreData(long from, long to) {
        if (!ApiModule.isNetworkAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.check_internet_connection), Toast.LENGTH_LONG).show();
            return;
        }

        mSpendingHistoryDetailedAdapter.startLoad();
        compositeDisposable.add(Single.zip(
                restAPI.getItemSpendingHistory(userProfile.getToken(), userProfile.getLineNumber(), mSpendingItemId, from, to, mRange),
                restAPI.hasAvailableSpendingItemHistory(userProfile.getToken(), userProfile.getLineNumber(), mSpendingItemId, from, to,mRange + 1),
                (spendingItemDetailedList, hasAvailableSpendingItemHistory) -> {
                    Map<String, Object> modelMap = new HashMap<>();
                    modelMap.put(Constants.SPENDING_DETAILED_ITEMS, spendingItemDetailedList);
                    modelMap.put(Constants.HAS_AVAILABLE_SPENDING_ITEM_HISTORY_FLAG_KEY,
                            hasAvailableSpendingItemHistory.get(Constants.HAS_AVAILABLE_SPENDING_ITEM_HISTORY_FLAG_KEY));
                    return modelMap;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getItemSpendingHistoryLoadMoreObserver()));
    }

    @Subscribe
    @Override
    public void onDatePickerDateSet(DatePickerDateSetEvent event) {
        super.onDatePickerDateSet(event);
    }

    private DisposableSingleObserver<Map<String, Object>> getItemSpendingHistoryObserver(long from, long to) {
        return new DisposableSingleObserver<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> modelMap) {
                handleSucceedItemSpendingHistoryObserving(modelMap, from, to);
            }

            @Override
            public void onError(Throwable e) {
                defaultHandleNetworkConnectionError(e);
            }
        };
    }

    private void handleSucceedItemSpendingHistoryObserving(Map<String, Object> modelMap, long from, long to) {
        List<SpendingItemDetailed> spendingItemDetailedList = (List<SpendingItemDetailed>) modelMap.get(Constants.SPENDING_DETAILED_ITEMS);
        mHasAvailableSpendingItemHistory = (boolean) modelMap.get(Constants.HAS_AVAILABLE_SPENDING_ITEM_HISTORY_FLAG_KEY);

        List<Object> items = groupByDateSpendingItemList(spendingItemDetailedList);

        if (mSpendingHistoryDetailedAdapter == null){
            mSpendingHistoryDetailedAdapter = new SpendingHistoryDetailedAdapter(items, mSpendingHistoryDetailedRecyclerView, Constants.VISIBLE_TRESHOLD);
            mSpendingHistoryDetailedRecyclerView.setAdapter(mSpendingHistoryDetailedAdapter);
        } else {
            mSpendingHistoryDetailedAdapter.updateData(items, false);
            mSpendingHistoryDetailedAdapter.activateListener(mSpendingHistoryDetailedRecyclerView);
        }

        mSpendingHistoryDetailedAdapter.setOnLoadMoreListener(() -> {
            if (mHasAvailableSpendingItemHistory) {
                loadMoreData(from, to);
            } else {
                mSpendingHistoryDetailedAdapter.deactivateListener(mSpendingHistoryDetailedRecyclerView);
            }
        });

        mFrom = from;
        mTo = to;
        if (mHasAvailableSpendingItemHistory) mRange = 2;

        mContentLayout.setVisibility(View.VISIBLE);
        //Stop loader
        hideLoader();
    }

    private DisposableSingleObserver<Map<String, Object>> getItemSpendingHistoryLoadMoreObserver() {
        return new DisposableSingleObserver<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> modelMap) {
                handleSucceedItemSpendingHistoryLoadMoreObserving(modelMap);
            }

            @Override
            public void onError(Throwable e) {
                mSpendingHistoryDetailedAdapter.stopLoad();
                defaultHandleNetworkConnectionError(e);
            }
        };
    }

    private void handleSucceedItemSpendingHistoryLoadMoreObserving(Map<String, Object> modelMap) {
        List<SpendingItemDetailed> spendingItemDetailedList = (List<SpendingItemDetailed>) modelMap.get(Constants.SPENDING_DETAILED_ITEMS);
        mHasAvailableSpendingItemHistory = (boolean) modelMap.get(Constants.HAS_AVAILABLE_SPENDING_ITEM_HISTORY_FLAG_KEY);

        List<Object> items = groupByDateSpendingItemList(spendingItemDetailedList);

        mSpendingHistoryDetailedAdapter.stopLoad();
        mSpendingHistoryDetailedAdapter.updateData(items, true);

        if (mHasAvailableSpendingItemHistory) mRange++;
    }

    private List<Object> groupByDateSpendingItemList(List<SpendingItemDetailed> spendingItemDetailedList) {
        List<Object> resultItems = new ArrayList<>();

        spendingItemDetailedList.sort((i1, i2) -> i2.getDate().compareTo(i1.getDate()));
        TreeMap<Timestamp, List<SpendingItemDetailed>> dateToItemMap = spendingItemDetailedList
                .stream()
                .collect(Collectors.groupingBy(SpendingItemDetailed::getDate,
                        () -> new TreeMap<>((o1, o2) -> o2.compareTo(o1)), toList()));
        dateToItemMap.entrySet().forEach(dateToItem -> {
            resultItems.add(dateToItem.getKey());
            resultItems.addAll(dateToItem.getValue());
        });
        return resultItems;
    }
}
