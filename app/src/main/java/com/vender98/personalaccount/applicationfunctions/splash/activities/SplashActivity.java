package com.vender98.personalaccount.applicationfunctions.splash.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.vender98.personalaccount.App;
import com.vender98.personalaccount.R;
import com.vender98.personalaccount.applicationfunctions.authentication.activities.AuthenticationActivity;
import com.vender98.personalaccount.applicationfunctions.dashboard.activities.DashboardActivity;
import com.vender98.personalaccount.dagger.modules.ApiModule;
import com.vender98.personalaccount.model.UserProfile;
import com.vender98.personalaccount.services.RestAPI;

import java.net.HttpURLConnection;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class SplashActivity extends AppCompatActivity {

    @Inject
    protected UserProfile userProfile;
    @Inject
    protected RestAPI restAPI;

    @BindView(R.id.loadingPanel)
    protected View mLoadingPanel;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);
        App.from(SplashActivity.this).appComponent().inject(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!ApiModule.isNetworkAvailable(SplashActivity.this)){
            Toast.makeText(SplashActivity.this, getString(R.string.check_internet_connection), Toast.LENGTH_LONG).show();
            return;
        }

        mLoadingPanel.setVisibility(View.VISIBLE);

        if (userProfile.isAuthorized()) {
            checkTokenIsValid();
        } else {
            startAuthenticationActivity();
        }
    }

    private void checkTokenIsValid() {
        compositeDisposable.add(restAPI.isTokenValid(userProfile.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getIsTokenValidObserver()));
    }

    private void startAuthenticationActivity() {
        Intent intent = new Intent(SplashActivity.this, AuthenticationActivity.class);
        startActivity(intent);
        finish();
    }

    private void startDashboardActivity() {
        Intent intent = new Intent(SplashActivity.this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    private DisposableCompletableObserver getIsTokenValidObserver() {
        return new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                startDashboardActivity();
            }

            @Override
            public void onError(Throwable e) {
                if (e instanceof HttpException) {
                    int code = ((HttpException) e).code();
                    if (code == HttpURLConnection.HTTP_FORBIDDEN) {
                        userProfile.cleanup();
                        startAuthenticationActivity();
                        return;
                    }
                }
                Toast.makeText(SplashActivity.this, getString(R.string.check_internet_connection), Toast.LENGTH_LONG).show();
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
