package com.vender98.personalaccount.applicationfunctions.spendinghistory.fragments;

import android.support.v4.app.DialogFragment;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.widget.Toast;

import com.vender98.personalaccount.R;
import com.vender98.personalaccount.constants.DatePickerOpenTime;
import com.vender98.personalaccount.events.DatePickerDateSetEvent;
import com.vender98.personalaccount.abstractfragments.BaseFragment;

import java.util.Calendar;

public abstract class SpendingHistoryAbstractFragment extends BaseFragment implements PopupMenu.OnMenuItemClickListener {

    private long fromTime;

    protected abstract void updateData(long from, long to);

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.calendar_popup_month_title:
                updateDataInTheLastMonth();
                break;
            case R.id.calendar_popup_week_title:
                updateDataInTheLastWeek();
                break;
            case R.id.calendar_popup_choose_title:
                openDatePicker(DatePickerOpenTime.FROM);
                break;
        }

        return true;
    }

    protected void updateDataInTheLastMonth() {
        Calendar calendar = Calendar.getInstance();
        long to = calendar.getTimeInMillis();
        calendar.add(Calendar.MONTH, -1);
        long from = calendar.getTimeInMillis();
        updateData(from, to);
    }

    protected void updateDataInTheLastWeek() {
        Calendar calendar = Calendar.getInstance();
        long to = calendar.getTimeInMillis();
        calendar.add(Calendar.WEEK_OF_MONTH, -1);
        long from = calendar.getTimeInMillis();
        updateData(from, to);
    }

    private void openDatePicker(DatePickerOpenTime openTime) {
        DialogFragment calendarFragment = DatePickerFragment.newInstance(openTime);
        calendarFragment.show(getChildFragmentManager(), getString(R.string.date_picker_tag));
        if (openTime == DatePickerOpenTime.FROM) {
            Toast.makeText(getContext(), getString(R.string.date_picker_show_first_time_message), Toast.LENGTH_LONG).show();
        }  else {
            Toast.makeText(getContext(), getString(R.string.date_picker_show_second_time_message), Toast.LENGTH_LONG).show();
        }
    }

    public void onDatePickerDateSet(DatePickerDateSetEvent event) {
        if (this.isResumed()) {
            if (event.getOpenTime() == DatePickerOpenTime.FROM) {
                fromTime = event.getFrom();
                openDatePicker(DatePickerOpenTime.TO);
            } else {
                updateData(fromTime, event.getTo());
            }
        }
    }

}
