package com.vender98.personalaccount.applicationfunctions.tariffplan.activities;

import android.support.v4.app.Fragment;

import com.vender98.personalaccount.R;
import com.vender98.personalaccount.abstractactivities.DoubleTabActivity;
import com.vender98.personalaccount.applicationfunctions.tariffplan.fragments.TariffPlanActiveFragment;
import com.vender98.personalaccount.applicationfunctions.tariffplan.fragments.TariffPlanAvailableFragment;

public class TariffPlansActivity extends DoubleTabActivity {
    @Override
    protected String getFirstTabTitle() {
        return getString(R.string.active_plan_title);
    }

    @Override
    protected Fragment getFirstTabFragment() {
        return TariffPlanActiveFragment.newInstance();
    }

    @Override
    protected String getSecondTabTitle() {
        return getString(R.string.available_plans_title);
    }

    @Override
    protected Fragment getSecondTabFragment() {
        return TariffPlanAvailableFragment.newInstance();
    }

    @Override
    protected int getActivityTitle() {
        return R.string.tariff_plans_activity_title;
    }
}
