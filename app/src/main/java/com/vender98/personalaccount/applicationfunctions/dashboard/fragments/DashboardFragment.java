package com.vender98.personalaccount.applicationfunctions.dashboard.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.vender98.personalaccount.R;
import com.vender98.personalaccount.abstractfragments.BaseFragment;
import com.vender98.personalaccount.applicationfunctions.authentication.activities.AuthenticationActivity;
import com.vender98.personalaccount.applicationfunctions.recharge.activities.RechargeActivity;
import com.vender98.personalaccount.applicationfunctions.spendinghistory.activities.SpendingHistoryActivity;
import com.vender98.personalaccount.applicationfunctions.tariffplan.activities.TariffPlansActivity;
import com.vender98.personalaccount.constants.Constants;
import com.vender98.personalaccount.dagger.modules.ApiModule;
import com.vender98.personalaccount.events.AmountChangedEvent;
import com.vender98.personalaccount.events.ChangeTariffPlanEvent;
import com.vender98.personalaccount.listeners.FragmentOnBackPressed;
import com.vender98.personalaccount.model.CustomerAccount;
import com.vender98.personalaccount.model.CustomerTariffPlan;
import com.vender98.personalaccount.model.Package;
import com.vender98.personalaccount.utils.Utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import lecho.lib.hellocharts.view.PieChartView;

public class DashboardFragment extends BaseFragment
    implements SwipeRefreshLayout.OnRefreshListener,
        NavigationView.OnNavigationItemSelectedListener,
        FragmentOnBackPressed {

    @BindView(R.id.customerName)
    protected TextView mCustomerName;
    @BindView(R.id.lineNumber)
    protected TextView mLineNumber;
    @BindView(R.id.balance)
    protected TextView mBalance;
    @BindView(R.id.rechargeButton)
    protected ImageView mRechargeButton;
    @BindView(R.id.packagesExpirePeriodLabel)
    protected TextView mPackagesExpirePeriodLabel;
    @BindView(R.id.minutesChart)
    protected PieChartView mMinutesChart;
    @BindView(R.id.trafficChart)
    protected PieChartView mTrafficChart;
    @BindView(R.id.smsChart)
    protected PieChartView mSmsChart;
    @BindView(R.id.refresh)
    protected SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.drawer)
    protected DrawerLayout mDrawerLayout;
    @BindView(R.id.nav_view)
    protected NavigationView mNavigationView;
    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);

        unbinder = ButterKnife.bind(this, v);
        mRefreshLayout.setOnRefreshListener(this);
        mNavigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, mToolbar, R.string.empty_string, R.string.empty_string);
        toggle.getDrawerArrowDrawable().setColor(Color.WHITE);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadDashboard(false);
    }

    private void loadDashboard(boolean refresh){
        if (!ApiModule.isNetworkAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.check_internet_connection), Toast.LENGTH_LONG).show();
            mRefreshLayout.setRefreshing(false);
            return;
        }

        if (refresh) {
            mRefreshLayout.setRefreshing(true);
        } else {
            showLoader();
        }

        compositeDisposable.add(Single.zip(
                        restAPI.getCustomerAccount(userProfile.getToken(), userProfile.getLineNumber()),
                        restAPI.getCustomerTariffPlan(userProfile.getToken(), userProfile.getLineNumber()),
                        (customerAccount, customerTariffPlan) -> {
                            Map<String, Object> modelMap = new HashMap<>();
                            modelMap.put(Constants.CUSTOMER_ACCOUNT_KEY, customerAccount);
                            modelMap.put(Constants.CUSTOMER_TARIFF_PLAN_KEY, customerTariffPlan);
                            return modelMap;
                        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(getDashboardDataObserver()));
    }

    private DisposableSingleObserver<Map<String, Object>> getDashboardDataObserver() {
        return new DisposableSingleObserver<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> modelMap) {
                handleSucceedDashboardDataObserving(modelMap);
            }

            @Override
            public void onError(Throwable e) {
                defaultHandleNetworkConnectionError(e);
                mRefreshLayout.setRefreshing(false);
            }
        };
    }

    private void handleSucceedDashboardDataObserving(Map<String, Object> modelMap) {
        CustomerAccount customerAccount = (CustomerAccount) modelMap.get(Constants.CUSTOMER_ACCOUNT_KEY);
        CustomerTariffPlan tariffPlan = (CustomerTariffPlan) modelMap.get(Constants.CUSTOMER_TARIFF_PLAN_KEY);

        initDashboardViews(customerAccount, tariffPlan);

        //Stop loaders
        hideLoader();
        mRefreshLayout.setRefreshing(false);
    }

    private void initDashboardViews(CustomerAccount customerAccount, CustomerTariffPlan tariffPlan) {
        mCustomerName.setText(customerAccount.getName());
        mLineNumber.setText(Utils.formatLineNumber(customerAccount.getLineNumber()));
        mBalance.setText(Utils.formatBalanceLabel(getString(R.string.amount_label), customerAccount.getBalance()));

        mPackagesExpirePeriodLabel.setText(String
                .format(getString(R.string.packages_expire_period_label),
                        Utils.getRemainingDaysAmount(tariffPlan.getConnectionDate())));

        tariffPlan.getPackages().forEach(pack -> {
            switch (pack.getType()) {
                case "Minutes":
                    initPackageChart(mMinutesChart, pack, getString(R.string.minutes_label));
                    break;
                case "Sms":
                    initPackageChart(mSmsChart, pack, getString(R.string.sms_label));
                    break;
                case "Megabytes":
                    initPackageChart(mTrafficChart, pack, getString(R.string.traffic_label));
                    break;

            }
        });
    }

    private void initPackageChart(PieChartView view, Package pack, String label) {
        int remainingCapacity = pack.getRemainingCapacity();
        int expiredCapacity = pack.getTotalCapacity() - remainingCapacity;
        List<Integer> chartValues = Arrays.asList(remainingCapacity, expiredCapacity);
        List<Integer> colors = Arrays.asList(Constants.REMAINING_PART_OF_CHART_COLOR, Constants.EXPIRED_PART_OF_CHART_COLOR);
        Utils.initChart(view, chartValues, colors, String.format(label, remainingCapacity));
    }

    @Override
    public void onRefresh() {
        loadDashboard(true);
    }

    @OnClick(R.id.rechargeButton)
    protected void onRechargeButtonClick(View view) {
        Intent intent = new Intent(getContext(), RechargeActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tariffButton)
    protected void onTariffButtonClick(View view) {
        Intent intent = new Intent(getContext(), TariffPlansActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.spendingHistoryButton)
    protected void onSpendingHistoryButtonClick(View view) {
        Intent intent = new Intent(getContext(), SpendingHistoryActivity.class);
        startActivity(intent);
    }

    @Subscribe
    public void onAmountChangedEvent(AmountChangedEvent event) {
        mBalance.setText(Utils.formatBalanceLabel(getString(R.string.amount_label), event.getAmount()));
    }

    @Subscribe
    public void onTariffPlanChangedEvent(ChangeTariffPlanEvent event) {
        CustomerTariffPlan tariffPlan = event.getTariffPlan();
        CustomerAccount customerAccount = event.getCustomerAccount();

        initDashboardViews(customerAccount, tariffPlan);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        switch (id) {
            case R.id.navigation_bar_exit:
                logout();
                break;
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle(R.string.logout_title)
                .setMessage(R.string.logout_message)
                .setPositiveButton( getString(R.string.logout_button), (dialog1, which) -> {
                    showLoader();
                    compositeDisposable.add(restAPI.logout(userProfile.getToken())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(getLogoutObserver()));
                })
                .setNegativeButton(getString(R.string.cancel_buton), (dialog1, which) -> dialog1.cancel())
                .create();
        dialog.setOnShowListener(dialogInterface -> {
            Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            positiveButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            positiveButton.setTextColor(Color.WHITE);
        });
        dialog.show();
    }

    private DisposableCompletableObserver getLogoutObserver() {
        return new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                onLogoutObservingResult();
            }

            @Override
            public void onError(Throwable e) {
                onLogoutObservingResult();
            }
        };
    }

    private void onLogoutObservingResult() {
        userProfile.cleanup();
        Intent intent = new Intent(getActivity(), AuthenticationActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public boolean onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return true;
        } else {
            return false;
        }
    }
}
