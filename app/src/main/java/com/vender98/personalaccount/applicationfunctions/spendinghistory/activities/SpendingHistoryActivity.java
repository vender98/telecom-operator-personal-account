package com.vender98.personalaccount.applicationfunctions.spendinghistory.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;

import com.vender98.personalaccount.R;
import com.vender98.personalaccount.abstractactivities.CalendarActivity;
import com.vender98.personalaccount.applicationfunctions.spendinghistory.fragments.SpendingHistoryFragment;

public class SpendingHistoryActivity extends CalendarActivity {

    @Override
    protected Fragment createFragment() {
        return SpendingHistoryFragment.newInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.spending_history_title);
        }
        initFragment();
    }
}
