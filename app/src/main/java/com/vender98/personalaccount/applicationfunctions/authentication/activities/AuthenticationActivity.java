package com.vender98.personalaccount.applicationfunctions.authentication.activities;

import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.vender98.personalaccount.R;
import com.vender98.personalaccount.abstractactivities.SingleFragmentActivity;
import com.vender98.personalaccount.applicationfunctions.authentication.fragments.AuthenticationFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthenticationActivity extends SingleFragmentActivity {

    @BindView(R.id.fragment_container)
    protected FrameLayout mFragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initFragment();
    }

    @Override
    protected Fragment createFragment() {
        return AuthenticationFragment.newInstance();
    }

    @Override
    public void onBackPressed() {
        ConstraintLayout phoneContainer = mFragmentContainer.findViewById(R.id.authentication_phone_container_layout);
        ConstraintLayout smsContainer = mFragmentContainer.findViewById(R.id.authentication_sms_container_layout);

        if (smsContainer.getVisibility() == View.VISIBLE) {
            smsContainer.setVisibility(View.GONE);
            phoneContainer.setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
        }
    }
}
