package com.vender98.personalaccount.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Vibrator;
import android.view.inputmethod.InputMethodManager;

import com.vender98.personalaccount.constants.Constants;

import java.sql.Timestamp;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

public class Utils {
    public static int getRemainingDaysAmount (Timestamp connectionDate) {
        return getTimeDifferenceWithConnectionDate(connectionDate);
    }

    private static int getTimeDifferenceWithConnectionDate(Timestamp connectionDate) {
        Calendar calendar = Calendar.getInstance();
        int currentDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        calendar.setTimeInMillis(connectionDate.getTime());
        int connectionDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        return connectionDayOfMonth >= currentDayOfMonth ?
                connectionDayOfMonth - currentDayOfMonth :
                connectionDayOfMonth - currentDayOfMonth + getDaysAmountInAMonth();
    }

    private static int getDaysAmountInAMonth() {
        Calendar calendar = Calendar.getInstance();
        long currentTime = calendar.getTimeInMillis();

        calendar.add(Calendar.MONTH, 1);
        long nextMonthTime = calendar.getTimeInMillis();

        return (int) ((nextMonthTime - currentTime) / 1000 / 60 / 60 / 24);
    }

    public static void initChart(PieChartView view, List<Integer> values, List<Integer> colors, String label) {
        view.setPieChartData(getChartData(values, colors, label));
        view.setChartRotation(Constants.CHART_ROTATION_ANGLE, false);
        view.setChartRotationEnabled(false);
        view.setClickable(false);
        view.setFocusable(false);
    }

    private static PieChartData getChartData(List<Integer> values, List<Integer> colors, String label) {

        List<SliceValue> pieData = new ArrayList<>();
        for (int i = 0; i < values.size(); ++i) {
            pieData.add(new SliceValue(values.get(i), colors.get(i)));
        }
        PieChartData pieChartData = new PieChartData(pieData);
        pieChartData
                .setHasCenterCircle(true)
                .setCenterText1(label)
                .setCenterText1FontSize(Constants.LABEL_CHART_FONT_SIZE)
                .setCenterText1Color(Constants.LABEL_CHART_COLOR);
        return pieChartData;
    }

    public static void hideSoftKeyboard (Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static String formatBalanceLabel(String label, int balance) {
        return String.format(label, new DecimalFormat(Constants.BALANCE_LABEL_PATTERN).format(balance));
    }

    public static String formatGeneralDate(Date date) {
        return formatDate(date, Constants.GENERAL_DATE_PATTERN);
    }

    public static String formatItemDate(Date date, String[] dateFormatSymbols) {
        return formatDate(date, Constants.ITEM_DATE_PATTERN, new DateFormatSymbols(){
            @Override
            public String[] getMonths() {
                return dateFormatSymbols;
            }
        });
    }

    public static String formatItemTime(Date time) {
        return formatDate(time, Constants.ITEM_TIME_PATTERN);
    }

    private static String formatDate(Date date, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        return dateFormat.format(date);
    }

    private static String formatDate(Date date, String pattern, DateFormatSymbols dateFormatSymbols) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, dateFormatSymbols);
        return dateFormat.format(date);
    }

    public static void vibrate(Context context, long millis) {
        if (context != null) {
            Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(1);
        }
    }

    public static void vibrate(Context context) {
        vibrate(context, 1);
    }

    public static String formatLineNumber(long lineNumber) {
        String stringifiedLineNumber = String.valueOf(lineNumber);

        return stringifiedLineNumber
                .replaceFirst("(\\d)(\\d{3})(\\d{3})(\\d{2})(\\d{2})", "+$1 ($2) $3-$4-$5");
    }

    public static long formatLineNumber(String lineNumber) {
        if (lineNumber.charAt(0) == '8')
            lineNumber = lineNumber.replaceFirst("8", "7");
        return Long.valueOf(lineNumber.replaceAll("\\D+",""));
    }

    public static boolean isLineNumberValid(String lineNumber) {
        return isMatches(lineNumber, "\\+7 \\(9(\\d{2})\\) (\\d{3})-(\\d{2})-(\\d{2})")
                || isMatches(lineNumber, "8 \\(9(\\d{2})\\) (\\d{3})-(\\d{2})-(\\d{2})");
    }

    public static boolean isSmsCodeValid(String code) {
        return isMatches(code, "(\\d{6})");
    }

    private static boolean isMatches(String s, String pattern) {
        return Pattern.compile(pattern).matcher(s).matches();
    }
}
