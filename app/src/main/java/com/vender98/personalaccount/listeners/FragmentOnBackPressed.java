package com.vender98.personalaccount.listeners;

public interface FragmentOnBackPressed {
    boolean onBackPressed();
}
