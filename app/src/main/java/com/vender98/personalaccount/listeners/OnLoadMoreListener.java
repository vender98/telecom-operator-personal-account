package com.vender98.personalaccount.listeners;

public interface OnLoadMoreListener {
    void onLoadMore();
}
