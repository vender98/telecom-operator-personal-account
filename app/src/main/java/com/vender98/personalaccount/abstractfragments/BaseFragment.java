package com.vender98.personalaccount.abstractfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import com.squareup.otto.Bus;
import com.vender98.personalaccount.App;
import com.vender98.personalaccount.R;
import com.vender98.personalaccount.model.UserProfile;
import com.vender98.personalaccount.services.RestAPI;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseFragment extends Fragment {

    @BindView(R.id.loadingPanel)
    protected View mLoadingPanel;

    protected CompositeDisposable compositeDisposable;

    @Inject
    protected RestAPI restAPI;
    @Inject
    protected UserProfile userProfile;
    @Inject
    protected Bus androidBus;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        App.from(context).appComponent().inject(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        compositeDisposable = new CompositeDisposable();
        androidBus.register(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
        androidBus.unregister(this);
    }

    protected void defaultHandleNetworkConnectionError(Throwable e) {
        handleApiError(e, getString(R.string.check_internet_connection));
    }

    protected void handleApiError(Throwable e, String message) {
        e.printStackTrace();
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
        hideLoader();
    }

    protected void showLoader() {
        mLoadingPanel.setVisibility(View.VISIBLE);
    }

    protected void hideLoader() {
        mLoadingPanel.setVisibility(View.GONE);
    }
}
