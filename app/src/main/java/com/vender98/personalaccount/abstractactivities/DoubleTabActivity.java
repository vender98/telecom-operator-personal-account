package com.vender98.personalaccount.abstractactivities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vender98.personalaccount.R;
import com.vender98.personalaccount.customviews.pagers.NonSwipableViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class DoubleTabActivity extends NavigateUpActivity {

    @BindView(R.id.double_tab_activity_viewpager)
    protected NonSwipableViewPager mViewPager;
    @BindView(R.id.double_tab_activity_tabs)
    protected TabLayout mTabLayout;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.double_tab_activity);
        unbinder = ButterKnife.bind(this);

        mViewPager.setAdapter(
                new DoubleTabActivityPagerAdapter(getSupportFragmentManager(),
                        getApplicationContext()));
        mViewPager.setPagingEnabled(false);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    protected abstract String getFirstTabTitle();

    protected abstract Fragment getFirstTabFragment();

    protected abstract String getSecondTabTitle();

    protected abstract Fragment getSecondTabFragment();

    private class DoubleTabActivityPagerAdapter extends FragmentPagerAdapter {

        private final int PAGE_COUNT = 2;
        private String tabTitles[] = new String[] { getFirstTabTitle(), getSecondTabTitle() };
        private Context context;

        DoubleTabActivityPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override public int getCount() {
            return PAGE_COUNT;
        }

        @Override public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return getFirstTabFragment();
                case 1:
                    return getSecondTabFragment();
                default:
                    return null;
            }
        }

        @Override public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }
}
