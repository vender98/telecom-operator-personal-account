package com.vender98.personalaccount;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;

import com.vender98.personalaccount.dagger.components.AppComponent;
import com.vender98.personalaccount.dagger.components.DaggerAppComponent;
import com.vender98.personalaccount.dagger.modules.AppModule;

public class App extends MultiDexApplication {
    private AppComponent appComponent;

    @NonNull
    public static App from(@NonNull Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = createAppComponent();
    }

    @NonNull
    private AppComponent createAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    @NonNull
    public AppComponent appComponent() {
        return appComponent;
    }
}
